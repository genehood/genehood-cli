import chai from 'chai';
import fs from 'fs'
import glob from 'glob'
import zlib from 'zlib'

const expect = chai.expect
const logLevel = 'warn'

import { Engine } from '../src/Engine'
import { IGeneHoodConfig, IGeneHoodData, IGeneHoodPack } from '../src/interfaces';

const kFixtures = {
  blastDbFileName: 'ghdb',
  filename: {
    blast: 'geneHood.blastp.temp.dat',
    config: 'geneHood.config.json',
    data: 'geneHood.data.json.gz',
    fasta: 'geneHood.fasta.temp.fa',
    pack: 'geneHood.pack.json.gz', 
  },
  initStep: 'fetchData',
  prefix: {
    altered: 'alteredSettings',
    alteredPrefix: 'alteredPrefix',
    badData: 'badData',
    buildMockPhylogeny: 'buildMockPhylogeny',
    buildPack: 'buildPack',
    diffStart: 'diffStart',
    fetchAseq: 'fetchAseq',
    fetchData: 'fetchData',
    init: 'init',
    makeBlastDB: 'makeBlastDB',
    makeFasta: 'makeFasta',
    parseBlastOutput: 'parseBlastOutput',
    runBlast: 'runBlast',
    stop: 'stop',
  },
}

const pkgJson = JSON.parse(fs.readFileSync('package.json').toString())

process.chdir('./dataTest')

describe('Engine', () => {
  class Mocked extends Engine {
    public setNextStep(step: string): this {
      return super.setNextStep(step)
    }
    public setNewickTree(tree: string): this {
      return super.setNewickTree(tree)
    }
    public buildMockPhylogeny(): this {
      return super.buildMockPhylogeny()
    }
    public async fetchData(): Promise<this> {
      return await super.fetchData()
    }
    public async fetchAseq(): Promise<this> {
      return await super.fetchAseq()
    }
    public makeFasta(): this {
      return super.makeFasta()
    }
    public async makeBlastDB(): Promise<this> {
      return await super.makeBlastDB()
    }
    public async runBlast(): Promise<this> {
      return await super.runBlast()
    }
    public async parseBlastOutput(): Promise<this> {
      return await super.parseBlastOutput()
    }
    public buildPack(): this {
      return super.buildPack()
    }
  }
  let mocked: any
  let data: any
  before(() => {
    mocked = new Mocked(logLevel)
  })
  describe('init', () => {
    const step = kFixtures.prefix.init
    const configFileName = `temp-${step}.${kFixtures.filename.config}`
    let engine: any
    before(async function () {
      this.timeout(60000)
      engine = new Engine(logLevel)
      engine.init(`temp-${step}`)
    })
    it('should make a config file with the configuration', () => {
      const genConfig = engine.getConfig()
      expect(genConfig.internal).to.exist
      expect(genConfig.user).to.exist
      expect(genConfig.history).to.exist
    })
    it('should put the cli version in internal section of the config', () => {
      const genConfig = engine.getConfig()
      expect(genConfig.internal.cliVersion).eql(pkgJson.version)
    })
    after(() => {
      const files = glob.sync('temp*')
      files.forEach((file) => {
        try {
          fs.unlinkSync(file)
        }
        catch(err) {
          return
        }
      })
    })
  })
  describe('load', () => {
    const step = kFixtures.prefix.fetchData
    before(async function () {
      this.timeout(60000)
      fs.writeFileSync(
        `temp-${step}.${kFixtures.filename.config}`,
        fs.readFileSync(`${step}.${kFixtures.filename.config}`)
      )
      fs.writeFileSync(
        `temp-${step}.${kFixtures.filename.data}`,
        fs.readFileSync(`${step}.${kFixtures.filename.data}`)
      )
    })
    it('should load an init config file with the configuration', () => {
      const engine = new Engine(logLevel)
      engine.load(`temp-${step}`)
      const expectedConfig = JSON.parse(fs.readFileSync(`temp-${step}.${kFixtures.filename.config}`).toString())
      expect(expectedConfig).eql(engine.getConfig())
    })
    it('should make sure to read new starting point from configuration', () => {
      const engine = new Engine(logLevel)
      engine.load(`temp-${step}`)
      const config = engine.getConfig()
      expect(config.user.startingStep).eql(config.internal.currentStep)
    })
    describe('Bad files', () => {
      const diff = kFixtures.prefix.diffStart
      before(async function () {
        this.timeout(60000)
        fs.writeFileSync(
          `temp-${diff}.${kFixtures.filename.config}`,
          fs.readFileSync(`${diff}.${kFixtures.filename.config}`)
        )
        fs.writeFileSync(
          `temp-${diff}.${kFixtures.filename.data}`,
          fs.readFileSync(`${diff}.${kFixtures.filename.data}`)
        )
      })
      it('should not read new starting point from configuration if passed to keep going', () => {
        const engine = new Engine(logLevel)
        engine.load(`temp-${diff}`, true)
        const expectedConfig = JSON.parse(fs.readFileSync(`${diff}.${kFixtures.filename.config}`).toString())
        const config = engine.getConfig()
        expect(config.internal.currentStep).eql(expectedConfig.internal.currentStep)
      })
      it('should make a fingerprint of user.settings if blank is provided', () => {
        const engine = new Engine(logLevel)
        engine.load(`temp-${diff}`, true)
        const config = engine.getConfig()
        const expectedConfig = JSON.parse(fs.readFileSync(`${diff}.${kFixtures.filename.config}`).toString())
        // tslint:disable-next-line: no-unused-expression
        expect(config.internal.hash).to.exist
        expect(expectedConfig.internal.hash).not.eql('')
      })
    })
    describe('Valid but changed', () => {
      const altered = kFixtures.prefix.altered
      before(async function () {
        this.timeout(60000)
        fs.writeFileSync(
          `temp-${altered}.${kFixtures.filename.config}`,
          fs.readFileSync(`${altered}.${kFixtures.filename.config}`)
        )
        fs.writeFileSync(
          `temp-${altered}.${kFixtures.filename.data}`,
          fs.readFileSync(`${altered}.${kFixtures.filename.data}`)
        )
      })
      it('should throw if user.settings do not match the hash', () => {
        const engine = new Engine(logLevel)
        expect(() => engine.load(`temp-${altered}`)).to.throw(`User settings can't be altered in the middle of the pipeline. Bad hash.`)
      })
    })
    describe('Valid but different prefix from project name', () => {
      const alteredPrefix = kFixtures.prefix.alteredPrefix
      before(async function () {
        this.timeout(60000)
        fs.writeFileSync(
          `temp-${alteredPrefix}.${kFixtures.filename.config}`,
          fs.readFileSync(`${alteredPrefix}.${kFixtures.filename.config}`)
        )
        fs.writeFileSync(
          `temp-${alteredPrefix}.${kFixtures.filename.data}`,
          fs.readFileSync(`${alteredPrefix}.${kFixtures.filename.data}`)
        )
      })
      it('should throw if user.settings do not match the hash', () => {
        const engine = new Engine(logLevel)
        expect(() => engine.load(`temp-${alteredPrefix}`)).to.throw(`Prefix in config does not match project name`)
      })
    })
    after(() => {
      const files = glob.sync('temp*')
      files.forEach((file) => {
        try {
          fs.unlinkSync(file)
        }
        catch(err) {
          return
        }
      })
    })
  })
  describe('skipping steps', () => {
    const step = kFixtures.prefix.fetchData
    before(async function () {
      this.timeout(60000)
      fs.writeFileSync(
        `temp-${step}.${kFixtures.filename.config}`,
        fs.readFileSync(`${step}.${kFixtures.filename.config}`)
      )
      fs.writeFileSync(
        `temp-${step}.${kFixtures.filename.data}`,
        fs.readFileSync(`${step}.${kFixtures.filename.data}`)
      )
      mocked.load(`temp-${step}`)
        .setNextStep('notTheRightStep')
      data = JSON.parse(zlib.gunzipSync(fs.readFileSync(`${kFixtures.prefix.init}.${kFixtures.filename.data}`)).toString()) as IGeneHoodData
    })
    it('fetchData should skip if currentStep is not right', async () => {
      await mocked.fetchData()
      const history = mocked.getConfig().history
      const fetchDataStamp = history.find((item: {step: string}) => item.step === 'fetchData')
      // tslint:disable-next-line: no-unused-expression
      expect(fetchDataStamp).to.be.undefined
    })
    it('fetchAseq should skip if currentStep is not right', async () => {
      await mocked.fetchAseq()
      const history = mocked.getConfig().history
      const fetchAseqStamp = history.find((item: {step: string}) => item.step === 'fetchAseq')
      // tslint:disable-next-line: no-unused-expression
      expect(fetchAseqStamp).to.be.undefined
    })
    it('makeFasta should skip if currentStep is not right', async () => {
      await mocked.makeFasta()
      const history = mocked.getConfig().history
      const makeFastaStamp = history.find((item: { step: string; }) => item.step === 'makeFasta')
      // tslint:disable-next-line: no-unused-expression
      expect(makeFastaStamp).to.be.undefined
    })
    it('makeBlastDB should skip if currentStep is not right', async () => {
      await mocked.makeBlastDB()
      const history = mocked.getConfig().history
      const makeBlastDBStamp = history.find((item: { step: string; }) => item.step === 'makeBlastDB')
      // tslint:disable-next-line: no-unused-expression
      expect(makeBlastDBStamp).to.be.undefined
    })
    it('runBlast should skip if currentStep is not right', async () => {
      await mocked.runBlast()
      const history = mocked.getConfig().history
      const runBlastStamp = history.find((item: { step: string; }) => item.step === 'runBlast')
      // tslint:disable-next-line: no-unused-expression
      expect(runBlastStamp).to.be.undefined
    })
    it('parseBlastOutput should skip if currentStep is not right', async () => {
      await mocked.parseBlastOutput()
      const history = mocked.getConfig().history
      const parseBlastOutputStamp = history.find((item: { step: string; }) => item.step === 'parseBlastOutput')
      // tslint:disable-next-line: no-unused-expression
      expect(parseBlastOutputStamp).to.be.undefined
    })
    it('buildMockPhylogeny should skip if nextStep is not right', async () => {
      await mocked.buildMockPhylogeny()
      const history = mocked.getConfig().history
      const parseBlastOutputStamp = history.find((item: { step: string; }) => item.step === 'buildMockPhylogeny')
      // tslint:disable-next-line: no-unused-expression
      expect(parseBlastOutputStamp).to.be.undefined
    })
    it('should skip if currentStep is not right', async () => {
      await mocked.buildPack()
      const history = mocked.getConfig().history
      const buildPackStamp = history.find((item: { step: string; }) => item.step === 'buildPack')
      // tslint:disable-next-line: no-unused-expression
      expect(buildPackStamp).to.be.undefined
    })
    after(() => {
      const files = glob.sync('temp*')
      files.forEach((file) => {
        try {
          fs.unlinkSync(file)
        }
        catch(err) {
          return
        }
      })
    })
  })
  describe('fetchData', () => {
    let step: any
    before(async function () {
      this.timeout(60000)
      step = kFixtures.prefix.fetchData
      fs.writeFileSync(
        `temp-${step}.${kFixtures.filename.config}`,
        fs.readFileSync(`${step}.${kFixtures.filename.config}`)
      )
      fs.writeFileSync(
        `temp-${step}.${kFixtures.filename.data}`,
        fs.readFileSync(`${step}.${kFixtures.filename.data}`)
      )
      mocked.load(`temp-${step}`)
      await mocked.fetchData()
      data = JSON.parse(zlib.gunzipSync(fs.readFileSync(`temp-${step}.${kFixtures.filename.data}`)).toString()) as IGeneHoodData
    })
    it('should have the right number of gene neighborhoods', () => {
      const expectedNumberOfGeneHoods = 4
      expect(data.gnl.length).eql(expectedNumberOfGeneHoods)
    })
    it('should have the right number of info about genomes', () => {
      const expectedNumberOfGenomes = 3
      expect(data.genomes.length).eql(expectedNumberOfGenomes)
    })
    it('should erase init data before running', async function() {
      this.timeout(60000)
      const mocked2 = new Mocked(logLevel)
      mocked2.load(`temp-${step}`)
      await mocked2.fetchData()
      const data2 = JSON.parse(zlib.gunzipSync(fs.readFileSync(`temp-${step}.${kFixtures.filename.data}`)).toString()) as IGeneHoodData
      const expectedNumberOfGeneHoods = 4
      expect(data2.gnl.length).eql(expectedNumberOfGeneHoods)
    })
    it('should set up to go to next step', () => {
      const expectedNextStep = 'fetchAseq'
      const nextStep = mocked.getConfig().internal.currentStep
      expect(nextStep).eql(expectedNextStep)
    })
    after(() => {
      const files = glob.sync('temp*')
      files.forEach((file) => {
        try {
          fs.unlinkSync(file)
        }
        catch(err) {
          return
        }
      })
    })
  })
  describe('fetchAseq', () => {
    let step: any
    before(async function () {
      this.timeout(60000)
      step = kFixtures.prefix.fetchAseq
      fs.writeFileSync(
        `temp-${step}.${kFixtures.filename.config}`,
        fs.readFileSync(`${step}.${kFixtures.filename.config}`)
      )
      fs.writeFileSync(
        `temp-${step}.${kFixtures.filename.data}`,
        fs.readFileSync(`${step}.${kFixtures.filename.data}`)
      )
      mocked.load(`temp-${step}`)
      await mocked.fetchAseq()
      data = JSON.parse(zlib.gunzipSync(fs.readFileSync(`temp-${step}.${kFixtures.filename.data}`)).toString()) as IGeneHoodData
    })
    it('should save the data and change the current step', () => {
      const config = mocked.getConfig()
      const expectedNumberOfAseqs = config.user.settings.stableIds.length * (config.user.settings.upstream + config.user.settings.downstream + 1)
      expect(data.aseqs.length).eql(expectedNumberOfAseqs)
    })
    it('should erase init data before running', async function() {
      this.timeout(60000)
      const mocked2 = new Mocked(logLevel)
      mocked2.load(`temp-${step}`)
      await mocked2.fetchAseq()
      const data2 = JSON.parse(zlib.gunzipSync(fs.readFileSync(`temp-${step}.${kFixtures.filename.data}`)).toString()) as IGeneHoodData
      const config = mocked.getConfig()
      const expectedNumberOfAseqs = config.user.settings.stableIds.length * (config.user.settings.upstream + config.user.settings.downstream + 1)
      expect(data2.aseqs.length).eql(expectedNumberOfAseqs)
    })
    it('should set up to go to next step', () => {
      const expectedNextStep = 'makeFasta'
      const nextStep = mocked.getConfig().internal.currentStep
      expect(nextStep).eql(expectedNextStep)
    })
    after(() => {
      const files = glob.sync('temp*')
      files.forEach((file) => {
        try {
          fs.unlinkSync(file)
        }
        catch(err) {
          return
        }
      })
    })
  })
  describe('makeFasta', () => {
    let step: any
    before(async function () {
      this.timeout(60000)
      step = kFixtures.prefix.makeFasta
      fs.writeFileSync(
        `temp-${step}.${kFixtures.filename.config}`,
        fs.readFileSync(`${step}.${kFixtures.filename.config}`)
      )
      fs.writeFileSync(
        `temp-${step}.${kFixtures.filename.data}`,
        fs.readFileSync(`${step}.${kFixtures.filename.data}`)
      )
      mocked.load(`temp-${step}`)
      await mocked.makeFasta()
    })
    it('should save the data and change the current step', () => {
      const fasta = fs.readFileSync(`temp-${step}.${kFixtures.filename.fasta}`).toString()
      const numberOfSequences = fasta.split('>').length - 1
      const config = mocked.getConfig()
      const expectedNumberOfSequences = config.user.settings.stableIds.length * (config.user.settings.upstream + config.user.settings.downstream + 1)
      expect(numberOfSequences).eql(expectedNumberOfSequences)
    })
    it('should set up to go to next step', () => {
      const expectedNextStep = 'makeBlastDB'
      const nextStep = mocked.getConfig().internal.currentStep
      expect(nextStep).eql(expectedNextStep)
    })
    after(() => {
      const files = glob.sync('temp*')
      files.forEach((file) => {
        try {
          fs.unlinkSync(file)
        }
        catch(err) {
          return
        }
      })
    })
  })
  describe('makeBlastDB', () => {
    let step: any
    before(async function () {
      step = kFixtures.prefix.makeBlastDB
      this.timeout(60000)
      fs.writeFileSync(
        `temp-${step}.${kFixtures.filename.config}`,
        fs.readFileSync(`${step}.${kFixtures.filename.config}`)
      )
      fs.writeFileSync(
        `temp-${step}.${kFixtures.filename.data}`,
        fs.readFileSync(`${step}.${kFixtures.filename.data}`)
      )
      fs.writeFileSync(
        `temp-${step}.${kFixtures.filename.fasta}`,
        fs.readFileSync(`${step}.${kFixtures.filename.fasta}`)
      )
      mocked.load(`temp-${step}`)
      await mocked.makeBlastDB()
    })
    it('should produce data and change the current step', async () => {
      const expectedFile = `temp-${step}.${kFixtures.blastDbFileName}.pin`
      const blastDbExists = fs.existsSync(expectedFile)
      // tslint:disable-next-line: no-unused-expression
      expect(blastDbExists, `It didn't build the file ${expectedFile}`).to.be.true
    })
    it('should set up to go to next step', () => {
      const expectedNextStep = 'runBlast'
      const nextStep = mocked.getConfig().internal.currentStep
      expect(nextStep).eql(expectedNextStep)
    })
    after(() => {
      const files = glob.sync('temp*')
      files.forEach((file) => {
        try {
          fs.unlinkSync(file)
        }
        catch(err) {
          return
        }
      })
    })
  })
  describe('runBlast', () => {
    let step: any
    before(async function () {
      step = kFixtures.prefix.runBlast
      this.timeout(60000)
      fs.writeFileSync(
        `temp-${step}.${kFixtures.filename.config}`,
        fs.readFileSync(`${step}.${kFixtures.filename.config}`)
      )
      fs.writeFileSync(
        `temp-${step}.${kFixtures.filename.data}`,
        fs.readFileSync(`${step}.${kFixtures.filename.data}`)
      )
      fs.writeFileSync(
        `temp-${step}.${kFixtures.filename.fasta}`,
        fs.readFileSync(`${step}.${kFixtures.filename.fasta}`)
      )
      fs.writeFileSync(
        `temp-${step}.${kFixtures.blastDbFileName}.pin`,
        fs.readFileSync(`${step}.${kFixtures.blastDbFileName}.pin`)
      )
      fs.writeFileSync(
        `temp-${step}.${kFixtures.blastDbFileName}.phr`,
        fs.readFileSync(`${step}.${kFixtures.blastDbFileName}.phr`)
      )
      fs.writeFileSync(
        `temp-${step}.${kFixtures.blastDbFileName}.psq`,
        fs.readFileSync(`${step}.${kFixtures.blastDbFileName}.psq`)
      )
      mocked.load(`temp-${step}`)
      await mocked.runBlast()
    })
    it('should produce data and change the current step', async () => {
      const blastDataExists = fs.existsSync(`temp-${step}.${kFixtures.filename.blast}`)
      // tslint:disable-next-line: no-unused-expression
      expect(blastDataExists).to.be.true
    })
    it('should set up to go to next step', () => {
      const expectedNextStep = 'parseBlastOutput'
      const nextStep = mocked.getConfig().internal.currentStep
      expect(nextStep).eql(expectedNextStep)
    })
    after(() => {
      const files = glob.sync('temp*')
      files.forEach((file) => {
        try {
          fs.unlinkSync(file)
        }
        catch(err) {
          return
        }
      })
    })
  })
  describe('parseBlastOutput', () => {
    let step: any
    before(async function () {
      step = kFixtures.prefix.parseBlastOutput
      this.timeout(60000)
      fs.writeFileSync(
        `temp-${step}.${kFixtures.filename.config}`,
        fs.readFileSync(`${step}.${kFixtures.filename.config}`)
      )
      fs.writeFileSync(
        `temp-${step}.${kFixtures.filename.data}`,
        fs.readFileSync(`${step}.${kFixtures.filename.data}`)
      )
      fs.writeFileSync(
        `temp-${step}.${kFixtures.filename.blast}`,
        fs.readFileSync(`${step}.${kFixtures.filename.blast}`)
      )
      mocked.load(`temp-${step}`)
      await mocked.parseBlastOutput()
      data = JSON.parse(zlib.gunzipSync(fs.readFileSync(`temp-${step}.${kFixtures.filename.data}`)).toString()) as IGeneHoodData
    })
    it('should produce data and change the current step', async () => {
     // tslint:disable-next-line: no-unused-expression
      expect(data.links).to.exist
      const config = mocked.getConfig()
      const GnlLength = data.gnl.length
      expect(GnlLength).eql(config.user.settings.stableIds.length)
    })
    it('should set up to go to next step', () => {
      const expectedNextStep = 'buildMockPhylogeny'
      const config = mocked.getConfig()
      const nextStep = config.internal.currentStep
      expect(nextStep).eql(expectedNextStep)
    })
    after(() => {
      const files = glob.sync('temp*')
      files.forEach((file) => {
        try {
          fs.unlinkSync(file)
        }
        catch(err) {
          return
        }
      })
    })
  })
  describe('buildMockPhylogeny', () => {
    let step: any
    before(async function () {
      step = kFixtures.prefix.buildMockPhylogeny
      this.timeout(60000)
      fs.writeFileSync(
        `temp-${step}.${kFixtures.filename.config}`,
        fs.readFileSync(`${step}.${kFixtures.filename.config}`)
      )
      fs.writeFileSync(
        `temp-${step}.${kFixtures.filename.data}`,
        fs.readFileSync(`${step}.${kFixtures.filename.data}`)
      )
      mocked.load(`temp-${step}`)
      await mocked.buildMockPhylogeny()
    })
    it('should build a politomy with the input genes as leaves', () => {
      const expected = "(GCF_000196175.1-BD_RS15585, GCF_000006765.1-PA1077, GCF_000006765.1-PA1105, GCF_000008485.1-lpg1216);"
      const tree = mocked.getConfig().user.newickTree
      expect(tree).eql(expected)
    })
    it('should skip if tree is in config', async () => {
      const newickTree= "(((GCF_000196175.1-BD_RS15585, GCF_000006765.1-PA1077), GCF_000006765.1-PA1105),GCF_000008485.1-lpg1216);"
      await mocked.load(`temp-${step}`)
        .setNewickTree(newickTree)
        .buildMockPhylogeny()
      const tree = mocked.getConfig().user.newickTree
      expect(tree).eql(newickTree)
    })
    it('should set up to go to next step', () => {
      const expectedNextStep = 'buildPack'
      const config = mocked.getConfig()
      const nextStep = config.internal.currentStep
      expect(nextStep).eql(expectedNextStep)
    })
    after(() => {
      const files = glob.sync('temp*')
      files.forEach((file) => {
        try {
          fs.unlinkSync(file)
        }
        catch(err) {
          return
        }
      })
    })
  })
  describe('buildPack', () => {
    let step: any
    before(async function () {
      step = kFixtures.prefix.buildPack
      this.timeout(60000)
      fs.writeFileSync(
        `temp-${step}.${kFixtures.filename.config}`,
        fs.readFileSync(`${step}.${kFixtures.filename.config}`)
      )
      fs.writeFileSync(
        `temp-${step}.${kFixtures.filename.data}`,
        fs.readFileSync(`${step}.${kFixtures.filename.data}`)
      )
      mocked.load(`temp-${step}`)
      await mocked.buildPack()
    })
    it('should produce data and change the current step', async () => {
      const packFileName = `temp-${step}.${kFixtures.filename.pack}`
      const dataPack = JSON.parse(zlib.gunzipSync(fs.readFileSync(packFileName)).toString()) as IGeneHoodPack
      // tslint:disable-next-line: no-unused-expression
      expect(dataPack.data).to.exist
      // tslint:disable-next-line: no-unused-expression
      expect(dataPack.config).to.exist
      fs.unlinkSync(packFileName)
    })
    it('should set up "" as next step', () => {
      const expectedNextStep = ''
      const config = mocked.getConfig()
      const nextStep = config.internal.currentStep
      expect(nextStep).eql(expectedNextStep)
    })
    after(() => {
      const files = glob.sync('temp*')
      files.forEach((file) => {
        try {
          fs.unlinkSync(file)
        }
        catch(err) {
          return
        }
      })
    })
  })
  describe('run', () => {
    let step: any
    before(async function () {
      step = kFixtures.prefix.fetchData
      this.timeout(60000)
      fs.writeFileSync(
        `temp-${step}.${kFixtures.filename.config}`,
        fs.readFileSync(`${step}.${kFixtures.filename.config}`)
      )
      fs.writeFileSync(
        `temp-${step}.${kFixtures.filename.data}`,
        fs.readFileSync(`${step}.${kFixtures.filename.data}`)
      )
    })
    it('should run the whole pipeline', async function () {
      this.timeout(60000)
      const engine = new Engine(logLevel)
      await engine.load(`temp-${step}`).run()
      const dataExist = fs.existsSync(`temp-${step}.${kFixtures.filename.data}`)
      // tslint:disable-next-line: no-unused-expression
      expect(dataExist).to.be.true
      const dataPack = JSON.parse(zlib.gunzipSync(fs.readFileSync(`temp-${step}.${kFixtures.filename.pack}`)).toString()) as IGeneHoodPack
      const config = engine.getConfig()
      const GnlLength = dataPack.data.gnl.length
      expect(GnlLength).eql(config.user.settings.stableIds.length)
    })
    after(() => {
      const files = glob.sync('temp*')
      files.forEach((file) => {
        try {
          fs.unlinkSync(file)
        }
        catch(err) {
          return
        }
      })
    })
  })
  describe('behaviors', () => {
    it('should stop pipeline in a given step', async function () {
      this.timeout(60000)
      const step = kFixtures.prefix.stop
      this.timeout(60000)
      fs.writeFileSync(
        `temp-${step}.${kFixtures.filename.config}`,
        fs.readFileSync(`${step}.${kFixtures.filename.config}`)
      )
      fs.writeFileSync(
        `temp-${step}.${kFixtures.filename.data}`,
        fs.readFileSync(`${step}.${kFixtures.filename.data}`)
      )
      const engine = new Engine(logLevel)
      await engine.load(`temp-${step}`).run()
      const pack = fs.existsSync(`temp-${step}.${kFixtures.filename.pack}`)
      // tslint:disable-next-line: no-unused-expression
      expect(pack).to.be.false
      const currentConfig = JSON.parse(fs.readFileSync(`temp-${step}.${kFixtures.filename.config}`).toString()) as IGeneHoodConfig
      expect(currentConfig.internal.currentStep).eql('makeBlastDB')
    })
    after(() => {
      const files = glob.sync('temp*')
      files.forEach((file) => {
        try {
          fs.unlinkSync(file)
        }
        catch(err) {
          return
        }
      })
    })
  })
})

#!/usr/bin/env node

// tslint:disable: no-console

import { ArgumentParser } from "argparse";
import chalk from "chalk";
import figlet from "figlet";
import fs from "fs";
import glob from "glob";
import { Logger, LogLevelDescType } from "loglevel-colored-prefix";
import { Engine } from "../Engine";
import { IGeneHoodConfig } from "../interfaces";

const logger = new Logger("info");
const log = logger.getLogger("genehood-cli");

// tslint:disable-next-line: no-var-requires no-require-imports
const pkjson = require("../../package.json");

const splash = figlet.textSync("GeneHood", {
  font: "Larry 3D",
  horizontalLayout: "fitted"
});

console.log(chalk.cyan(splash));
console.log(`\t\t\t\t\t\t\t\t  ${chalk.cyan("" + pkjson.version)}`);
console.log(chalk.red("\t\t\t\t\t\t\t by Davi Ortega"));

const parser = new ArgumentParser({
  addHelp: true,
  description:
    "Command line application to generate the data for Gene neighborhood visualization.",
  prog: "genehood"
});

parser.addArgument("project", {
  help: "Name of the project file"
});

parser.addArgument("--action", {
  choices: ["init", "run", "keepGoing", "cleanUp"],
  help:
    "Choose between initialize the GeneHood in this directory, run it with an existing configuration from the step given by the user or keepGoing from where it stopped."
});

parser.addArgument("--addPhylogeny", {
  help: "add custom phylogeny to the project",
  nargs: 1
});

parser.addArgument("--addStableIds", {
  help: "add stableIds from file to the project. One stableId per line.",
  nargs: 1
});

parser.addArgument("--addRange", {
  help:
    "add range for gene neighborhood. Number of genes Downstream followed by upstream.",
  nargs: 2,
  type: Number
});

parser.addArgument("--logLevel", {
  choices: ["quiet", "verbose"],
  help:
    "Makes GeneHood to be quieter and only log warns and errors or verbose with debug level logs."
});

const args = parser.parseArgs();

const logLevel = !args.logLevel
  ? "info"
  : args.logLevel === "quiet"
  ? "warn"
  : "debug";

const genehood = new Engine(logLevel);
if (args.action === "init") {
  genehood.init(args.project);
} else if (args.addPhylogeny || args.addRange || args.addStableIds) {
  if (args.addPhylogeny) {
    const config = JSON.parse(
      fs.readFileSync(`${args.project}.geneHood.config.json`).toString()
    ) as IGeneHoodConfig;
    log.info(`Adding phylogeny.`);
    config.user.newickTree = fs.readFileSync(args.addPhylogeny[0]).toString();
    fs.writeFileSync(
      `${args.project}.geneHood.config.json`,
      JSON.stringify(config, null, " ")
    );
    const packExists = fs.existsSync(`${args.project}.geneHood.pack.json.gz`);
    if (packExists) {
      config.user.startingStep = "buildMockPhylogeny";
      fs.writeFileSync(
        `${args.project}.geneHood.config.json`,
        JSON.stringify(config, null, " ")
      );
      genehood
        .load(args.project)
        .run()
        .catch(err => {
          throw err;
        });
    }
  }
  if (args.addStableIds) {
    log.info(`Adding stable ids from file: ${args.addStableIds[0]}`);
    const stableIds = fs
      .readFileSync(args.addStableIds[0])
      .toString()
      .split("\n")
      .filter(id => id !== "");
    log.info(`Adding ${stableIds.length} ids.`);
    const config = JSON.parse(
      fs.readFileSync(`${args.project}.geneHood.config.json`).toString()
    ) as IGeneHoodConfig;
    config.user.settings.stableIds = stableIds;
    fs.writeFileSync(
      `${args.project}.geneHood.config.json`,
      JSON.stringify(config, null, " ")
    );
  }
  if (args.addRange) {
    const config = JSON.parse(
      fs.readFileSync(`${args.project}.geneHood.config.json`).toString()
    ) as IGeneHoodConfig;
    const downstream = args.addRange[0];
    const upstream = args.addRange[1];
    if (upstream && downstream) {
      config.user.settings.downstream = downstream;
      config.user.settings.upstream = upstream;
      log.info(`Adding new range`);
      log.info(`Downstream: ${downstream}`);
      log.info(`Upstream ${upstream}`);
      fs.writeFileSync(
        `${args.project}.geneHood.config.json`,
        JSON.stringify(config, null, " ")
      );
    } else {
      log.error(`Range must be integer numbers.`);
      log.error(`Downstream: ${downstream}`);
      log.error(`Upstream ${upstream}`);
    }
  }
} else if (args.action === "cleanUp") {
  fs.unlinkSync(`${args.project}.geneHood.data.json.gz`);
  fs.unlinkSync(`${args.project}.geneHood.fasta.temp.fa`);
  fs.unlinkSync(`${args.project}.geneHood.blastp.temp.dat`);
  const blastDbFiles = glob.sync(`${args.project}.ghdb.*`);
  blastDbFiles.forEach(file => {
    fs.unlinkSync(file);
  });
} else {
  const keepGoing = args.action === "keepGoing";
  genehood
    .load(args.project, keepGoing)
    .run()
    .catch(err => {
      throw err;
    });
}

import { ILink } from "blastinutils-ts/lib/interfaces";

interface IGeneHoodConfigUserSettings {
  stableIds: string[];
  geneHoodPrefix: string;
  upstream: number;
  downstream: number;
}

interface IGeneHoodConfigUserInput {
  newickTree: string;
  settings: IGeneHoodConfigUserSettings;
  startingStep: string;
  stopStep: string;
}

interface IGeneHoodConfigInternal {
  cliVersion: string;
  currentStep: string;
  hash: string;
}

interface IHistory {
  step: string;
  date: string;
}

interface IGeneHood {
  order: number[];
  ref: string;
  refStrand: "+" | "-";
  pos: {
    stop: number;
    start: number;
    refStart: number;
  };
}

interface IGeneHoodData {
  genes: any[];
  genomes: any[];
  gnl: IGeneHood[];
  aseqs: any[];
  links: ILink[];
}

interface IGeneHoodConfig {
  user: IGeneHoodConfigUserInput;
  internal: IGeneHoodConfigInternal;
  history: IHistory[];
}

interface IGeneHoodPack {
  config: IGeneHoodConfig;
  data: IGeneHoodData;
}

export { IGeneHoodConfig, IGeneHoodData, IGeneHoodPack, IGeneHood };

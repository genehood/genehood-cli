import { BioSeq, BioSeqSet, FastaUtils } from "bioseq-ts";
import childProcess from "child_process";
import crypto from "crypto";
import fs from "fs";
import { promisify } from "util";
import zlib from "zlib";

import { Logger, LogLevelDescType } from "loglevel-colored-prefix";
import { Aseq, Genes, Genome } from "mist3-ts";

import { CommandToolKit, interfaces } from "blastinutils-ts";
import {
  ISupportedParams,
  WritableParser
} from "blastinutils-ts/lib/BlastToLinksStream";
import { IMakeBlastDBParameters } from "blastinutils-ts/lib/interfaces";

import {
  IGeneHood,
  IGeneHoodConfig,
  IGeneHoodData,
  IGeneHoodPack
} from "./interfaces";

// tslint:disable-next-line: no-var-requires no-require-imports
const pkgJson = require("../package.json");

const kDefaults = {
  blastDBName: "ghdb",
  geneHoodBlastFileName: "geneHood.blastp.temp.dat",
  geneHoodConfigFileName: "geneHood.config.json",
  geneHoodDataFileName: "geneHood.data.json.gz",
  geneHoodFastaFileName: "geneHood.fasta.temp.fa",
  geneHoodPackFileName: "geneHood.pack.json.gz",
  hashAlgo: "sha256",
  initStep: "fetchData"
};

const blastParams: interfaces.IBlastPParameters = {
  evalue: 1,
  num_threads: 4,
  outfmt: {
    format: 6,
    parameters: ["qseqid", "sseqid", "bitscore", "pident", "evalue", "length"]
  }
};

const makeBlastDBParams: IMakeBlastDBParameters = {
  dbtype: "prot"
};

/**
 * The class that builds a GeneHood dataset
 *
 * @class Engine
 */
class Engine {
  private config: IGeneHoodConfig;
  private data: IGeneHoodData;
  private log: Logger;
  private logLevel: LogLevelDescType;

  constructor(logLevel: LogLevelDescType = "INFO") {
    // stableIdFile, phyloFile = null, packedFile = 'geneHood.pack.json', tempFastaFile = 'geneHood.fa', tempGeneFile = 'geneHood.gene.json') {
    const minimalConfig: IGeneHoodConfig = {
      history: [],
      internal: {
        cliVersion: pkgJson.version,
        currentStep: kDefaults.initStep,
        hash: ""
      },
      user: {
        newickTree: "",
        settings: {
          downstream: 2,
          geneHoodPrefix: "",
          stableIds: [],
          upstream: 2
        },
        startingStep: kDefaults.initStep,
        stopStep: ""
      }
    };
    this.data = {
      aseqs: [],
      genes: [],
      genomes: [],
      gnl: [],
      links: []
    };
    this.config = minimalConfig;
    this.logLevel = logLevel;
    this.log = new Logger(logLevel);
    const log = this.log.getLogger("GeneHood::Constructor");
    log.debug(`Engine Class instance has been built. This is the config:`);
    log.debug(this.config);
  }

  /**
   * Initializes a minimal config file and saves it with given prefix.
   *
   * @param {string} prefix
   * @returns
   * @memberof Engine
   */
  public init(prefix: string) {
    this.config.user.settings.geneHoodPrefix = prefix;
    this.addToHistory("init").updateFiles();
    return this;
  }

  /**
   * Load existing configuration and data based on prefix.
   *
   * @param {string} prefix
   * @returns
   * @memberof Engine
   */
  public load(prefix: string, keepGoing: boolean = false) {
    const log = this.log.getLogger("GeneHood::load");
    log.info(`Loading existing config:`);
    const configFileName = [prefix, kDefaults.geneHoodConfigFileName].join(".");
    const dataFileName = [prefix, kDefaults.geneHoodDataFileName].join(".");
    const config = JSON.parse(fs.readFileSync(configFileName).toString());
    if (this.isIGeneHoodConfig(config)) {
      this.config = config;
    }
    log.debug(this.config);
    if (this.config.user.settings.geneHoodPrefix !== prefix) {
      log.error(`Prefix in config does not match project name`);
      throw Error(`Prefix in config does not match project name`);
    }
    const data = JSON.parse(
      zlib.gunzipSync(fs.readFileSync(dataFileName)).toString()
    ) as IGeneHoodData;
    if (this.isIGeneHoodData(data)) {
      this.data = data;
    }
    if (!keepGoing) {
      this.config.internal.currentStep = this.config.user.startingStep;
    }
    if (!this.config.internal.hash) {
      log.info('Found no fingerprint for "user.settings". Making one now.');
      const userSetting = JSON.stringify(this.config.user.settings);
      const hashPwd = this.makeHash(userSetting);
      this.config.internal.hash = hashPwd;
    } else {
      log.info("Found fingerpring. Checking user.settings");
      const currSetting = JSON.stringify(this.config.user.settings);
      const currHash = this.makeHash(currSetting);
      log.debug(`Hash: ${currHash}`);
      if (currHash !== this.config.internal.hash) {
        log.error(
          `User settings can't be altered in the middle of the pipeline. Bad hash.`
        );
        log.error(`Expected hash: ${this.config.internal.hash}`);
        log.error(`Calculated hash: ${currHash}`);
        throw new Error(
          `User settings can't be altered in the middle of the pipeline. Bad hash.`
        );
      }
    }
    this.updateFiles();
    return this;
  }

  /**
   * Returns the current state of configuration
   *
   * @returns
   * @memberof Engine
   */
  public getConfig() {
    return this.config;
  }

  /**
   * Runs the GeneHood pipeline.
   *
   * @returns
   * @memberof Engine
   */
  public async run() {
    const log = this.log.getLogger("GeneHood::run");
    log.info("Running GeneHood pipeline");
    return this.fetchData()
      .then(() => this.fetchAseq())
      .then(() => this.makeFasta())
      .then(() => this.makeBlastDB())
      .then(() => this.runBlast())
      .then(() => this.parseBlastOutput())
      .then(() => this.buildMockPhylogeny())
      .then(() => this.buildPack())
      .catch((err: Error) => {
        log.debug("Handling exception");
        if (err.message === "PipelineInterruption") {
          log.warn("Stopping the pipeline");
        } else {
          throw err;
        }
      })
      .then(() => {
        log.info("All done");
      });
  }

  /**
   * Changes the current step in the config file
   *
   * @protected
   * @param {string} step
   * @returns
   * @memberof Engine
   */
  protected setNextStep(step: string) {
    this.config.internal.currentStep = step;
    return this;
  }

  /**
   * Set a new newick tree in the config file
   *
   * @protected
   * @param {string} tree
   * @returns
   * @memberof Engine
   */
  protected setNewickTree(tree: string) {
    this.config.user.newickTree = tree;
    return this;
  }

  /**
   * Saves config and data files
   *
   * @memberof Engine
   */
  protected updateFiles() {
    const log = this.log.getLogger("GeneHood::updateFiles");
    const configFileName = [
      this.config.user.settings.geneHoodPrefix,
      kDefaults.geneHoodConfigFileName
    ].join(".");
    const dataFileName = [
      this.config.user.settings.geneHoodPrefix,
      kDefaults.geneHoodDataFileName
    ].join(".");
    log.info(`Saving updated config file: ${configFileName}`);
    fs.writeFileSync(configFileName, JSON.stringify(this.config, null, " "));
    log.info(`Saving updated zipped data file: ${dataFileName}`);
    const buf = zlib.gzipSync(JSON.stringify(this.data, null, " "));
    fs.writeFileSync(dataFileName, buf);
    return this;
  }

  /**
   * Fetch relevant information from MiST3
   *
   * @protected
   * @returns
   * @memberof Engine
   */
  protected async fetchData() {
    const log = this.log.getLogger("GeneHood::fetchData");
    log.info("Starting fetchData step");
    if (this.config.internal.currentStep !== "fetchData") {
      log.info(
        `Skipping fetchData step because of config step is: ${this.config.internal.currentStep}`
      );
      return this;
    }
    log.warn(`This will erase any existing data from previous runs.`);
    this.data.gnl = [];
    this.data.genes = [];
    const genomeVersions: Set<string> = new Set([]);
    const mist3 = new Genes(this.logLevel);
    const processed: string[] = [];
    const genes: any[] = [];
    for (const stableId of this.config.user.settings.stableIds) {
      const gnRaw = await mist3.getGeneHood(
        stableId,
        this.config.user.settings.upstream,
        this.config.user.settings.downstream
      );
      const gn: IGeneHood = {
        order: [],
        pos: {
          refStart: 0,
          start: 0,
          stop: 0
        },
        ref: "",
        refStrand: "+"
      };
      let gnStart = Infinity;
      let gnStop = -Infinity;
      gnRaw.forEach(
        (gene: {
          stable_id: string;
          strand: "+" | "-";
          start: number;
          stop: number;
        }) => {
          const sid = gene.stable_id;
          if (sid === stableId) {
            gn.ref = sid;
            gn.refStrand = gene.strand;
            gn.pos.refStart = gn.refStrand === "+" ? gene.start : gene.stop;
            genomeVersions.add(gn.ref.split("-")[0]);
          }
          gnStart = gnStart < gene.start ? gnStart : gene.start;
          gnStop = gnStop > gene.stop ? gnStop : gene.stop;
          const index = processed.indexOf(sid);
          if (index === -1) {
            processed.push(sid);
            genes.push(gene);
            gn.order.push(processed.length - 1);
          } else {
            gn.order.push(index);
          }
        }
      );

      gn.pos.start =
        (gn.refStrand === "+" ? gnStart : gnStop) - gn.pos.refStart;
      gn.pos.stop = (gn.refStrand === "+" ? gnStop : gnStart) - gn.pos.refStart;
      this.data.gnl.push(gn);
    }
    log.info(`Adding genome info`);
    const genomeMist3 = new Genome();
    log.debug(genomeVersions);
    for (const version of genomeVersions) {
      const genomeInfo = await genomeMist3.fetchDetails(version);
      this.data.genomes.push(genomeInfo);
    }
    this.data.genes = genes;
    const stop = this.setNextStep("fetchAseq")
      .addToHistory("fetchData")
      .updateFiles()
      .checkStop("fetchData");
    if (stop) {
      log.info(`Pipeline interrupted by user request at step fetchData`);
      throw new Error("PipelineInterruption");
    }
    log.debug(`Done saving the file`);
    return this;
  }

  /**
   * Fetch Aseq info for the genes in list.
   *
   * @protected
   * @returns
   * @memberof Engine
   */
  protected async fetchAseq() {
    const log = this.log.getLogger("GeneHood::fetchAseq");
    log.info("Starting fetchAseq step");
    if (this.config.internal.currentStep !== "fetchAseq") {
      log.info(
        `Skipping fetchAseq step because of config step is: ${this.config.internal.currentStep}`
      );
      return this;
    }
    const aseqList = this.data.genes
      .filter(gene => gene.aseq_id !== null)
      .map(gene => gene.aseq_id);
    const aseq = new Aseq(this.logLevel);
    const aseqInfoList = await aseq.fetchMany(aseqList);
    this.data.aseqs = aseqInfoList;
    const stop = this.setNextStep("makeFasta")
      .addToHistory("fetchAseq")
      .updateFiles()
      .checkStop("fetchAseq");
    if (stop) {
      log.info(`Pipeline interrupted by user request at step fetchAseq`);
      throw new Error("PipelineInterruption");
    }
    log.debug(`Done saving the file`);
    return this;
  }

  /**
   * Make fasta file with all genes with known protein sequence
   *
   * @protected
   * @returns
   * @memberof Engine
   */
  protected makeFasta() {
    const log = this.log.getLogger("GeneHood::makeFasta");
    log.info("Starting makeFasta step");
    if (this.config.internal.currentStep !== "makeFasta") {
      log.info(
        `Skipping makeFasta step because of config step is: ${this.config.internal.currentStep}`
      );
      return this;
    }
    const bioSeqList: BioSeq[] = [];
    this.data.genes.forEach(gene => {
      const header = gene.stable_id;
      const aseq = this.data.aseqs.find(a => a.id === gene.aseq_id);
      if (aseq) {
        const sequence = aseq.sequence;
        const bioSeq = new BioSeq(header, sequence);
        bioSeqList.push(bioSeq);
      } else {
        log.warn(
          `Gene ${gene.stable_id} has no protein sequence in the MiST3 database.`
        );
      }
    });
    const bioSeqSet = new BioSeqSet(bioSeqList);
    const fu = new FastaUtils();
    const fasta = fu.write(bioSeqSet);
    const fastaFileName = [
      this.config.user.settings.geneHoodPrefix,
      kDefaults.geneHoodFastaFileName
    ].join(".");

    fs.writeFileSync(fastaFileName, fasta);

    const stop = this.setNextStep("makeBlastDB")
      .addToHistory("makeFasta")
      .updateFiles()
      .checkStop("makeFasta");
    if (stop) {
      log.info(`Pipeline interrupted by user request at step makeFasta`);
      throw new Error("PipelineInterruption");
    }
    return this;
  }

  /**
   * Makes the Blast Database
   *
   * @private
   * @returns
   * @memberof Engine
   */
  protected async makeBlastDB() {
    const log = this.log.getLogger("GeneHood::makeBlastDB");
    log.info("Starting makeBlastBD step");
    if (this.config.internal.currentStep !== "makeBlastDB") {
      log.info(
        `Skipping makeBlastDB step because of config step is: ${this.config.internal.currentStep}`
      );
      return this;
    }
    const params = makeBlastDBParams;
    params.in = [
      this.config.user.settings.geneHoodPrefix,
      kDefaults.geneHoodFastaFileName
    ].join(".");
    params.out = [
      this.config.user.settings.geneHoodPrefix,
      kDefaults.blastDBName
    ].join(".");
    const ctk = new CommandToolKit(this.logLevel);
    const command = ctk.build("makeblastdb", params);
    await this.runCommand(command);
    const stop = this.setNextStep("runBlast")
      .addToHistory("makeBlastDB")
      .updateFiles()
      .checkStop("makeBlastDB");
    if (stop) {
      log.info(`Pipeline interrupted by user request at step makeBlastDB`);
      throw new Error("PipelineInterruption");
    }
    return this;
  }

  /**
   * Run Blast on the dataset
   *
   * @protected
   * @returns
   * @memberof Engine
   */
  protected async runBlast() {
    const log = this.log.getLogger("GeneHood::runBlast");
    log.info("Starting runBlast step");
    if (this.config.internal.currentStep !== "runBlast") {
      log.info(
        `Skipping runBlast step because of config step is: ${this.config.internal.currentStep}`
      );
      return this;
    }
    const params = blastParams;
    params.db = [
      this.config.user.settings.geneHoodPrefix,
      kDefaults.blastDBName
    ].join(".");
    params.out = [
      this.config.user.settings.geneHoodPrefix,
      kDefaults.geneHoodBlastFileName
    ].join(".");
    params.query = [
      this.config.user.settings.geneHoodPrefix,
      kDefaults.geneHoodFastaFileName
    ].join(".");
    const ctk = new CommandToolKit(this.logLevel);
    const command = ctk.build("blastp", params);
    await this.runCommand(command);
    const stop = this.setNextStep("parseBlastOutput")
      .addToHistory("runBlast")
      .updateFiles()
      .checkStop("runBlast");
    if (stop) {
      log.info(`Pipeline interrupted by user request at step runBlast`);
      throw new Error("PipelineInterruption");
    }
    return this;
  }

  /**
   * Parse Blast output into nodes and links (d3js style)
   *
   * @protected
   * @returns
   * @memberof Engine
   */
  protected async parseBlastOutput(): Promise<this> {
    const log = this.log.getLogger("GeneHood::parseBlastOutput");
    log.info("Starting parseBlastOutput step");
    const params: ISupportedParams = blastParams.outfmt as ISupportedParams;
    const index = this.data.genes.map(gene => gene.stable_id);
    const parser = new WritableParser(index, params, this.logLevel);
    const blastFileName = [
      this.config.user.settings.geneHoodPrefix,
      kDefaults.geneHoodBlastFileName
    ].join(".");

    const parse = () => {
      return new Promise((resolve, reject) => {
        fs.createReadStream(blastFileName)
          .pipe(parser)
          .on("finish", () => {
            log.info("All parsed");
            const data = parser.getData();
            this.data.links = data;
            resolve();
            return;
          })
          .on("error", (err: any) => {
            log.error(err);
            reject(err);
            return;
          });
      });
    };

    if (this.config.internal.currentStep !== "parseBlastOutput") {
      log.info(
        `Skipping parseBlastOutput step because of config step is: ${this.config.internal.currentStep}`
      );
      return this;
    }
    await parse();
    const stop = this.setNextStep("buildMockPhylogeny")
      .addToHistory("parseBlastOutput")
      .updateFiles()
      .checkStop("parseBlastOutput");
    if (stop) {
      log.info(`Pipeline interrupted by user request at step parseBlastOutput`);
      throw new Error("PipelineInterruption");
    }

    return this;
  }

  /**
   * Build a polytomy to replace the lack of phylogenetic information between the main genes or clusters
   *
   * @protected
   * @returns
   * @memberof Engine
   */
  protected buildMockPhylogeny() {
    const log = this.log.getLogger("GeneHood::buildMockPhylogeny");
    log.info("Starting buildMockPhylogeny step");
    if (this.config.internal.currentStep !== "buildMockPhylogeny") {
      log.info(
        `Skipping buildMockPhylogeny step because of config step is: ${this.config.internal.currentStep}`
      );
      return this;
    }
    log.debug(`Currently in the config: "${this.config.user.newickTree}"`);
    if (this.config.user.newickTree === "") {
      log.info(`No Phylogenetic tree found in config. Let's mock one.`);
      this.config.user.newickTree = `(${this.config.user.settings.stableIds.join(
        ", "
      )});`;
      this.addToHistory("builMockPhylogeny");
    }
    const stop = this.setNextStep("buildPack")
      .addToHistory("buildMockPhylogeny")
      .updateFiles()
      .checkStop("buildMockPhylogeny");
    if (stop) {
      log.info(
        `Pipeline interrupted by user request at step buildMockPhylogeny`
      );
      throw new Error("PipelineInterruption");
    }
    return this;
  }

  /**
   * Build the final data pack for the viewer
   *
   * @protected
   * @returns
   * @memberof Engine
   */
  protected buildPack() {
    const log = this.log.getLogger("GeneHood::buildPack");
    log.info("Starting buildPack step");
    if (this.config.internal.currentStep !== "buildPack") {
      log.info(
        `Skipping buildPack step because of config step is: ${this.config.internal.currentStep}`
      );
      return this;
    }
    const packFileName = [
      this.config.user.settings.geneHoodPrefix,
      kDefaults.geneHoodPackFileName
    ].join(".");
    const pack: IGeneHoodPack = {
      config: this.config,
      data: this.data
    };
    log.info(
      `Writing final pack for ${this.config.user.settings.geneHoodPrefix} to ${packFileName}`
    );
    const buf = zlib.gzipSync(JSON.stringify(pack));
    fs.writeFileSync(packFileName, buf);
    const stop = this.setNextStep("")
      .addToHistory("buildPack")
      .updateFiles()
      .checkStop("buildPack");
    if (stop) {
      log.info(`Pipeline interrupted by user request at step buildPack`);
      throw new Error("PipelineInterruption");
    }
    log.info("Done. All set.");
    return this;
  }

  /**
   * Register in history when the step was completed
   *
   * @private
   * @param {string} step
   * @memberof Engine
   */
  private addToHistory(step: string) {
    const now = Date();
    const item = this.config.history.find(record => record.step === step);
    if (item) {
      item.date = Date();
    } else {
      this.config.history.push({
        date: now,
        step
      });
    }
    return this;
  }

  /**
   * Helper function to launch third party cli programs.
   *
   * @private
   * @param {string} command
   * @returns
   * @memberof Engine
   */
  private async runCommand(command: string) {
    const log = this.log.getLogger("GeneHood::runComand");
    const exec = promisify(childProcess.exec);

    const { stdout, stderr } = await exec(command);
    if (stderr) {
      log.error(stderr);
      throw new Error(stderr);
    }
    if (stdout) {
      log.info(stdout);
    }
    return this;
  }

  private makeHash(data: string) {
    return crypto
      .createHash(kDefaults.hashAlgo)
      .update(data)
      .digest("hex");
  }

  /**
   * Check if pipeline should stop in this step.
   *
   * @private
   * @param {string} step
   * @returns
   * @memberof Engine
   */
  private checkStop(step: string) {
    return this.config.user.stopStep === step;
  }

  /**
   * Type check for config files
   *
   * @private
   * @param {*} candidate
   * @returns {candidate is IGeneHoodConfig}
   * @memberof Engine
   */
  private isIGeneHoodConfig(candidate: any): candidate is IGeneHoodConfig {
    if (candidate.user === undefined) {
      throw new Error('Data corrupted: missing "user" field');
    }
    if (candidate.internal === undefined) {
      throw new Error('Data corrupted: missing "internal" field');
    }
    if (candidate.history === undefined) {
      throw new Error('Data corrupted: missing "history" field');
    }
    return true;
  }

  /**
   * Type check for data files
   *
   * @private
   * @param {*} candidate
   * @returns {candidate is IGeneHoodData}
   * @memberof Engine
   */
  private isIGeneHoodData(candidate: any): candidate is IGeneHoodData {
    const log = this.log.getLogger("GeneHood::isIGeneHoodData");
    if (candidate.genes === undefined) {
      throw new Error('Data corrupted: missing "genes" field');
    }
    if (candidate.genomes === undefined) {
      throw new Error('Data corrupted: missing "genomes" field');
    }
    if (candidate.gnl === undefined) {
      throw new Error('Data corrupted: missing "gnl" field');
    }
    if (candidate.aseqs === undefined) {
      throw new Error('Data corrupted: missing "aseqs" field');
    }
    if (candidate.links === undefined) {
      throw new Error('Data corrupted: missing "links" field');
    }
    return true;
  }
}

export { Engine };
